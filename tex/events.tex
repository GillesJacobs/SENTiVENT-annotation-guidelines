This chapter describes the annotation of events, argument fillers and event co-reference (in the form of Event Hoppers).

\section{What is an event?}

\subsubsection{Conceptually: events in economic news}

An event is a textual description of a real-world occurrence that involves multiple participants.
Generally, it describes a change of state in the world.
An event describes what has happened, who was involved, at what time, in which place.
Deals, employee changes, product launches, elections, company mergers or lawsuits are the some intuitive examples of what constitutes an event in economic news. 

\subsubsection{Technically: features of an event}

Technically, an event is always described explicitly in the text:
First, the presence of an event is indicated by a lexical \textbf{trigger.}
Second, each event belongs to a certain \textbf{type and subtype}.
Events outside the typology are not tagged.
We count \typecount types and \subtypecount subtypes (see appendix \fullref{chapter/eventtype}. 
Third, we want to find the arguments (people, companies, products, etc.) that participate in the event.
Fourth, we give the event a \textbf{realis} value that indicates if the event has actually happened or not.
We also perform event co-reference to link event mentions to each other if they refer to the same event.

In the following sections, we explain these elements in detail.

\section{Event triggers}

The trigger of an event is the \textbf{minimal span of text} (a single word or a small phrase) that most succinctly expresses the occurrence of an event.
It is often the main verb describing an action or a state.
Generally, we think of the trigger as the word that most strongly refers to an event.
In the examples below (and throughout this document), event triggers are \anntrg{bold}.
We also indicate the \type{type and subtype} of most events in the example sentences.

\begin{exe}
    \ex\label{ex/verb1} \annexe{On Monday, shares of biopharmaceutical company Celgene \anntrg{tumbled}}.
        \expl the \textbf{verb} \anntrg{tumbled} is the trigger of a \type{SecurityValue} event.
    \ex\label{ex/noun1} Noun: \annexe{[..] AA's exclusive airline \anntrg{sponsorship deal} with the World Series champion Cubs.}
        \expl the noun \anntrg{sponsorship deal} is the trigger of a \type{CSR/Brand} event.
    \ex\label{ex/noun2} \annexe{Viscen is revealed to be the \anntrg{buyer} of the ACX directories.}
        \expl the \textbf{noun} \anntrg{buyer} is the trigger of a \type {MergerAcquisition} event.
\end{exe}

\subsection{Event trigger extent: What forms do event triggers take?}
As examples \ref{ex/verb1} and \ref{ex/noun1} show, the trigger can be a \textbf{verb}, but also a \textbf{noun}, \textbf{pronoun} or a past or present \textbf{participle} or \textbf{adjective} in modifier position.

\begin{exe}
    \ex\label{ex/verb2} Verb: \annexe{The FDA did not \anntrg{approve} JNJ's new medicine.}
    \ex\label{ex/noun3} Noun: \annexe{The \anntrg{acquisition} of ACX went over without a problem.}
    \ex\label{ex/adjective} Adjective: \annexe{The \anntrg{banktrupt} firm left investors angry.}
    \ex\label{ex/pastparticiple} Past-participle: \annexe{The company was \anntrg{litigated} against on grounds of workfloor safety violations.}
\end{exe}

\textbf{Resultative events}:
We typically think of events as processes or actions; but we also tag states that result from taggable events.
As shown by the examples below, resultative events can be predicate adjectives, participles used as modifiers or even present participles that denote an action currently in progress.

\begin{exe}
    \ex\label{ex/predicateadjective} Predicate adjective: \annexe{The firm is \anntrg{bankrupt.}}
    \ex\label{ex/npadjective} Nominal modifier adjective: \annexe{The \anntrg{bankrupt} firm leaves many angry investors behind.}
    \ex\label{ex/presentparticiple} Present participle: \annexe{The firms are currently \anntrg{merging}.}
\end{exe}

As resultative states these examples can be paraphrased as "the state of having gone bankrupt" or "the state of having been merged".
Always tag both on-going events and resultative events.

\textbf{Event nominalizations and pronominalizations}:
Nominal events can also occur as premodifiers in a noun phrase.
In this case, only the noun that refers to the event is tagged:

\begin{exe}
    \ex \annexe{Quaker Oats rejected PepsiCo's \anntrg{takeover} offer as too low.}
        \expl \type{MergerAcquisition}
    \ex \annexe{In April of last year, the CR Company began \anntrg{bankruptcy} procedures}.
        \expl \type{Bankruptcy}
    \ex \annexe{Biogen's Alzheimer drug began the clinical \anntrg{testing} phase}.
        \expl \type{ProductService.Trail}
\end{exe}

Anaphors of events such as \textbf{pronouns and definite descriptions} of previously mentioned events are also tagged.

\begin{exe}
    \ex\label{ex/pronoun1} Pronoun: \annexe{The firm went \anntrg{bankrupt.} \anntrg{It} was a great loss for many of the early-stage investors.}
        \expl pronoun \anntrg{It} refers to a previous \type{Bankruptcy} event and is tagged.
    \ex\label{ex/pronoun2} Definite noun phrase: \annexe{Amazon \anntrg{launched} its own smartphone. \anntrg{It} was a festive \anntrg{affair}.}
        \expl pronoun \anntrg{It} and definite noun \anntrg{affair} refers to a previous event \type{Product.Launch}.
\end{exe}

Anaphoric triggers, i.e. \anntrg{it} and \anntrg{affair} do not require argument fillers.
They are the same type and subtype as the event they refer.

% Comes from official rERE gl's p 11.

\subsection{Complex triggers: Finding the right words}

Identifying the trigger of events is often straightforward, as in example \ref{ex/mainverb} above.
Just as often, we find a number of words that could be marked as a trigger, or an event is described in such a way that picking a single word as a trigger does not feel right.
\textbf{As a rule of thumb, we keep triggers as small as possible} without it losing its event type or subtype meaning.
In this section, we describe procedures to find the right trigger when it is not obvious.

Practically, annotators read the full article text using the event typology as a guiding reference.
During first reading(s) they note possible events mentioned in the article.
We advice annotators to focus on identifying types first and only assign subtypes after triggers have been found.

Next they attentively go over the article a second time and looking for the lexical triggers.
Noting the triggers, annotators double check their spans.

% \subsubsection{Triggers as contiguous groups of words}

% Is the trigger an uninterrupted group of words?

% An event can be described by a group of words such that it is impossible to pick one word without losing the meaning of the phrase.
% In that case, the trigger is the entire phrase.
% Like many aspects of annotation, this is often subjective; we encourage annotators to use their best judgment based on the examples in this document -- keeping in mind that triggers should not be longer than they absolutely need to be.

% \begin{exe}
%     \ex \annexe{Hoe zijn de Spaanse autoriteiten de Catalaanse ex-minister-president Carles Puigdemont \anntrg{op het spoor gekomen?}}
%         \expl \type{Justice.Investigation}
%     \ex \annexe{De soldaten \anntrg{zijn weer thuis.}}
%         \expl None of the words in this group by itself carry the meaning of movement.
%         \expl \type{Movement.TransportPerson}
% \end{exe}

\subsubsection{Picking a word from multiple possible trigger words}

There may still be situations where you can reasonably identify multiple different words for a single event trigger. We provide a few rules in these cases to avoid confusion. As a general rule-of-thumb: Always select the smallest meaningful lexical unit as an event trigger.
\\[10pt]
\noindent\textbf{The Stand-Alone Noun Rule}:
In \textbf{verb+noun} constructions, we will simply select the noun whenever that noun can be used by itself to refer to the event.
If the verb+noun cannot be reduced without loosing the event meaning multiple words will be tagged.

\begin{exe}
    \ex \annexe{Foo Corp. had previously \textit{filed} \anntrg{Chapter 11} in 2001.}
        \expl the \textbf{noun} \anntrg{Chapter 11} not verb+noun \textit{filed Chapter 11} is the trigger as per the Stand-Alone Noun Rule.
    \ex \annexe{The company had to \textit{pay a} \anntrg{fine} of 300.000EUR.}
        \expl the \textbf{fine} \anntrg{Chapter 11} not verb+noun \textit{pay a fine} is the trigger as per the Stand-Alone Noun Rule.
\end{exe}

\noindent\textbf{Stand-Alone Adjective Rule}:
In \textbf{verb+X+noun} constructions, when a verb and an adjective are used together to express the occurrence of an event, the adjective will be chosen as the trigger whenever it can stand alone to express the resulting state brought about by the event.

\begin{exe}
    \ex \annexe{The negative findings left 3 projects \anntrg{disapproved}.}
        \expl the \textbf{adjective} \anntrg{disapproved} not verb+X+adjective \textit{left 3 projects} is the trigger as per the Stand-Alone Adjective Rule.
\end{exe}

\noindent\textbf{Main Verb Rule}: When several verbs are used to together to express an event, only the main verb is the trigger.
\begin{exe}
    \ex \annexe{XYZ Corp. \textit{announced} \annexe{laying off} 37 workers in the Chicago facility.}
    \ex \annexe{John D. Idol will \anntrg{take over} as Chief Executive.}
    \ex \annexe{XYZ Corp \anntrg{laid} Jane off.}
    \ex \annexe{John D. Idol had \anntrg{taken} the company over.}
\end{exe}

\noindent\textbf{Contiguous Verb+Particle/Verb+Adverb Rule}: In \textbf{verb+particle and verb+adverb} constructions we will tag main verb and particle together only if the words occur contiguously. If they are interrupted we only annotate the verb.
\begin{exe}
    \ex \annexe{Jane was \anntrg{laid off} by XYZ Corp.}
    \ex \annexe{John D. Idol will \anntrg{take over} as Chief Executive.}
    \ex \annexe{XYZ Corp \anntrg{laid} Jane off.}
    \ex \annexe{John D. Idol had \anntrg{taken} the company over.}
\end{exe}

\subsection{Multiple events within a sentence}
Do not confuse cases where there multiple possible triggers for the same event within the same sentence with cases where there multiple events expressed in the same sentence.
Multiple events can be expressed in the same sentence.

This usually both in complex sentences, i.e. with coordinated (\textit{and, or, but, for, etc.}) and subordinated (if, that, because, where, etc.) clauses.
But also in simplex sentences consisting of one clause.

\begin{exe}
    \ex \annexe{The \anntrg{product launch} caused a rise in \anntrg{revenue} and \anntrg{sales}.}
    \ex \annexe{John D. Idol will \anntrg{take over} as Chief Executive.}
\end{exe}

Sometimes multiple events are triggered by adjectives sharing the same verb. Tag each adjective as a separate event.

% \section{Realis}

% The Realis attribute indicates how `real' the event is. \textbf{Actual} events are attested to have occurred in real life; the author asserts with certainty that they have happened. Most events you will find are Actual. \textbf{Generic} events also actually happen, but they do not refer to single specific events. They describe things that happen without referring to a specific instance. For instance, in example \ref{ex/generic_realis}, the act of smuggling is habitual and not specific. Finally, a trigger might describe an event that will happen in the future, or might happen at some point, or might have happened in the past, or someone suggests something might happen or has happened. These type of events, along with negated events, go into the realis category \textbf{Other}.

% Events that are reported according to some source (as in example \ref{ex/volgens_politie}) also carry a realis attribute of \textbf{Other}.
% % I checked the ACE guidelines for this but they have no info at all.

% \begin{exe}
%     \ex \annexe{Provincie Oost-Vlaanderen geeft \anntrg{miljoenencadeau} aan projectontwikkelaar. }
%         \expl \type{Trans\-action.\-Trans\-fer\-Money, Realis = Actual.}
    
%     \ex\label{ex/generic_realis} \annexe{Drugs worden vaak de grens \anntrg{overgesmokkeld}.}
%         \expl \type{Movement.TransportArtifact, Realis = Generic.}

%     \ex\label{ex/volgens_politie} \annexe{Volgens politiebronnen \anntrg{schept} de dader op in de kliniek.}
%         \expl \type{Contact.Broad\-cast, Realis = Other.} \annexe{Volgens politiebronnen} indicates that the event is a claim by police sources. A claim is not sufficient to tag this event as Actual.
    
%     \ex \annexe{President Trump \anntrg{dreigt} ermee Saipov naar Guantanamo te \anntrg{sturen}. }
%         \expl \annexe{Dreigt:} \type{Contact.Broadcast, Realis = Actual.} 
%         \expl \annexe{Sturen:} \type{Movement.\-Trans\-port\-Personnel, Realis = Other.} The first event indicates the second is only something that might happen in the future.
    
%     \ex \annexe{Uiteindelijk werd het geld helemaal niet \anntrg{overgebracht}. }
%         \expl \type{Transaction.Trans\-fer\-Money, Realis = Other.} Negated events go into Other.
% \end{exe}

\section{Dealing with quotations}

We annotate inside a quotation the same way we annotate outside of it. We do not consider the quotation to be a separate sentence. That means it is possible to identify an event outside a quotation and annotate arguments for that event inside of it, and vice-versa.

\begin{exe}
    \ex \annexe{\emph{De politie} liet hem weten dat \emph{hij} "snel \anntrg{gearresteerd} zou worden".}
        \expl \type{Justice.ArrestJail}
        \expl Agent = De politie
        \expl Person = hij
        \expl The Person argument lies outside the quote.
\end{exe}